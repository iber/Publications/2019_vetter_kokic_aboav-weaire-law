% This function calculates the double integral for m given n using the
% acceptance-rejection method with a uniform grid.
function m = m_irregular_numeric(param, n)
    N1 = 1000;
    Ni = 1000;
    m = NaN(size(n));
    theta_low = param(1);
    theta_high = param(2);
    for j = 1:length(n)
        theta_n = 180*(1-2/n(j));
        theta_i_neg = -param(3)*360/n(j);
        theta_i_pos =  param(4)*360/n(j);
        theta_i = linspace(theta_i_neg, theta_i_pos, Ni);
        mlist = NaN(1,Ni*N1);
        for i = 1:Ni
            theta_1 = linspace(theta_low, theta_high, N1);
            theta_2 = 360 - theta_n - theta_i(i) - theta_1;
            theta_2(theta_2<theta_low) = NaN;
            theta_2(theta_2>theta_high) = NaN;
            n1 = 360./(180-theta_1);
            n2 = 360./(180-theta_2);
            mlist((i-1)*N1+1:(i*N1)) = (n1+n2)/2;
        end
        m(j) = nanmean(mlist(:));
    end
end