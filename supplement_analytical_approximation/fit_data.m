% This script fits the irregular approximation of AW's law to the lines
% defined by m*n=a*n+b with a and b values obtained for all available
% tissue samples and writes the resulting parameters theta_high and beta
% including their standard errors to the file.

AWparams = readtable('AWparams.csv');

fitted_params = NaN(height(AWparams),4);
n = 3:12;
for i = 1:height(AWparams)
    model = fit_irregular(n, AWparams.a(i)*n+AWparams.b(i))
    param = model.Coefficients.Estimate;
    se = diag(sqrt(model.CoefficientCovariance))';
    AWparams.theta_max(i) = param(1);
    AWparams.se_theta_max(i) = se(1);
    AWparams.beta(i) = param(2);
    AWparams.se_beta(i) = se(2);
end
writetable(AWparams, 'AWparams.csv')
