% This script calculates the integration bounds theta_min and theta_max as
% a function of the AW coefficients a and b, plots them as surfaces, and
% writes them to raw text files.

afunc = @(x,y) 180/(y-x)*log((180-x)/(180-y)*y/x);
bfunc = @(x,y) 2*(180/x)*(180/y);

options = optimset('TolFun', 1e-10, 'TolX', 1e-10, 'MaxFunEvals', 1e6);

a = 4.15:0.05:6;
b = 6:0.1:10;
theta_min =  60 * ones(length(a), length(b));
theta_max = 150 * ones(length(a), length(b));

for i = 1:length(a)
    for j = 1:length(b)
        t = fminsearch(@(t) norm([a(i)-afunc(t(1),t(2)); b(j)-bfunc(t(1),t(2))]), [theta_min(i,max(1,j-1)) theta_max(i,max(1,j-1))], options);
        theta_min(i,j) = t(1);
        theta_max(i,j) = t(2);
    end
end

theta_min = theta_min';
theta_max = theta_max';

[agrid, bgrid] = meshgrid(a, b);
d1 = [agrid(:) bgrid(:), theta_min(:)];
d2 = [agrid(:) bgrid(:), theta_max(:)];
save('theta_min.txt', 'd1', '-ascii')
save('theta_max.txt', 'd2', '-ascii')

figure
surf(agrid, bgrid, theta_min);
xlabel('a')
ylabel('b')
zlabel('\theta_{min}')

figure
surf(agrid, bgrid, theta_max);
xlabel('a')
ylabel('b')
zlabel('\theta_{max}')
